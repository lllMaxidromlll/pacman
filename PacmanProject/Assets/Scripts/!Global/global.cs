﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class global
{
    public static System.Random random = new System.Random();
    public static float Speed = 4.000F;
    public static Maze Maze;
    public static IPath Path;

    public static uint DotsLeft;

    public static Transform camera = Camera.main.transform;
    public static Transform PlayerTransform;

    public static event Action NextMapEvent;
    public static event Action StartEvent;
    public static event Action PauseEvent;
    public static event Action ResetEvent;

    public static event Action DotEatEvent;
    public static event Action EnergizerEatEvent;
    public static event Action FruitEatEvent;
    public static event Action PlayerDiedEvent;
    public static event Action<GameObject> GhostDiedEvent;

    public static void NextMap()
    {
        if (NextMapEvent != null)
            NextMapEvent();
    }
    public static void StartMap()
    {
        if (StartEvent != null)
            StartEvent();
    }
    public static void PauseMap()
    {
        if (PauseEvent != null)
            PauseEvent();
    }
    public static void ResetMap()
    {
        if (ResetEvent != null)
            ResetEvent();
    }

    public static void DotEat()
    {
        if (DotEatEvent != null)
            DotEatEvent();
    }
    public static void EnergizerEat()
    {
        if (EnergizerEatEvent != null)
            EnergizerEatEvent();
    }
    public static void FruitEat()
    {
        if (FruitEatEvent != null)
            FruitEatEvent();
    }
    public static void PlayerDied()
    {
        if (PlayerDiedEvent != null)
            PlayerDiedEvent();
    }
    public static void GhostDied(GameObject ghost)
    {
        if (GhostDiedEvent != null)
            GhostDiedEvent(ghost);
    }
}
