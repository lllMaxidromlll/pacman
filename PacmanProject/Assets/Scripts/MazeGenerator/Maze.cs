﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class Maze : MonoBehaviour, MGinterfaces.IMaze
{
    public TextAsset TemplateFile;
    public GameObject WallPrefab;
    public GameObject TunelPrefab;
    public GameObject DotPrefab;
    public GameObject EnergizerPrefab;

    MGinterfaces.IMaze maze;
    ConcreteBuilder builder;
    ConcreteMazeGenerator generator;

    public int Width
    {
        get
        {
            return maze.Width;
        }
    }
    public int Height
    {
        get
        {
            return maze.Height;
        }
    }
    public Vector2 PlayerRespawn
    {
        get { return maze.PlayerRespawn; }
        set
        {
            maze.PlayerRespawn = value;
        }
    }
    public List<Vector2> GhostRespawn
    {
        get { return maze.GhostRespawn; }
        set
        {
            maze.GhostRespawn = value;
        }
    }
    public Vector2 Tunel1
    {
        get { return maze.Tunel1; }
        set
        {
            maze.Tunel1 = value;
        }
    }
    public Vector2 Tunel2
    {
        get { return maze.Tunel2; }
        set
        {
            maze.Tunel2 = value;
        }
    }
    public Vector2[] corners
    {
        get
        {
            return maze.corners;
        }
        set
        {
            maze.corners = value;
        }
    }

    public MGinterfaces.EGridType GetType(int x, int y)
    {
        return maze.GetType(x, y);
    }
    public MGinterfaces.EGridType GetType(Vector2 cell)
    {
        return maze.GetType(cell);
    }
    public void Build ()
    {
        List<string> lines = TemplateFile.text.Split('\r', '\n').ToList();
        lines.RemoveAll(p => (p.Equals("\r") || p.Equals("\n") || p.Equals("")));
        builder = new ConcreteBuilder(lines, new ConcreteFigureBuilder());
        generator = new ConcreteMazeGenerator(builder);
        maze = generator.Construct();
        if(WallPrefab != null)
        {
            for(int y = 0; y < maze.Height; y++)
            {
                for (int x = 0; x < maze.Width; x++)
                {
                    if (maze.GetType(x, y) == MGinterfaces.EGridType.Wall)
                        Instantiate(WallPrefab, new Vector2(x, y).ToVector3(), Quaternion.Euler(Vector3.zero)).transform.SetParent(this.transform);
                }
            }
        }
        if(TunelPrefab != null)
        {
            Vector2 tunel1 = maze.Tunel1;
            Vector2 tunel2 = maze.Tunel2;
            Transform t1 = Instantiate(TunelPrefab, new Vector2(tunel1.x, tunel1.y).ToVector3(), Quaternion.Euler(Vector3.zero)).transform;
            t1.SetParent(this.transform);
            Transform t2 = Instantiate(TunelPrefab, new Vector2(tunel2.x, tunel2.y).ToVector3(), Quaternion.Euler(Vector3.zero)).transform;
            t2.SetParent(this.transform);
            Transform leftUp, rightDown;
            if(t1.position.x * t1.position.y > t2.position.x * t2.position.y)
            {
                leftUp = t1;
                rightDown = t2;
            }else
            {
                leftUp = t2;
                rightDown = t1;
            }
            if(leftUp.position.x > leftUp.position.y)
                leftUp.Rotate(new Vector3(0, -90.0F, 0));
            else
                leftUp.Rotate(new Vector3(-90.0F, 0, 0));
            if (rightDown.position.x > rightDown.position.y)
                rightDown.Rotate(new Vector3(0, 90.0F, 0));
            else
                rightDown.Rotate(new Vector3(90.0F, 0, 0));
        }
        if(DotPrefab != null)
        {
            for (int y = 0; y < maze.Height; y++)
            {
                for (int x = 0; x < maze.Width; x++)
                {
                    if (maze.GetType(x, y) == MGinterfaces.EGridType.Dot)
                        Instantiate(DotPrefab, new Vector2(x, y).ToVector3(), Quaternion.Euler(Vector3.zero)).transform.SetParent(this.transform);
                }
            }
        }
        if (EnergizerPrefab != null)
        {
            int cornerCounter = 0;
            ((MGinterfaces.IMaze)maze).corners = new Vector2[4];
            for (int y = 0; y < maze.Height; y++)
            {
                for (int x = 0; x < maze.Width; x++)
                {
                    if (maze.GetType(x, y) == MGinterfaces.EGridType.Energizer)
                    {
                        Instantiate(EnergizerPrefab, new Vector2(x, y).ToVector3(), Quaternion.Euler(Vector3.zero)).transform.SetParent(this.transform);
                        ((MGinterfaces.IMaze)maze).corners[cornerCounter++] = new Vector2(x, y);
                    }
                }
            }
        }
    }
    public GameObject Clone()
    {
        GameObject newObject = MonoBehaviour.Instantiate(gameObject);
        Maze newMaze = newObject.GetComponent<Maze>();
        newMaze.builder = this.builder;
        newObject.GetComponent<Maze>().maze = ((MGinterfaces.IMazeBuilder < ConcreteMaze > )(newMaze.builder)).GetMaze();
        return newObject;
    }
}
