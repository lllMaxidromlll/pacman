﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using UnityEngine;
using System;

public class ConcreteMaze : MGinterfaces.IMaze
{
    MGinterfaces.EGridType[,] grid;
    Vector2 playerRespawn;
    List<Vector2> ghostRespawn;
    Vector2 tunel1;
    Vector2 tunel2;
    Vector2[] corners;

    public ConcreteMaze(int columns, int rows)
    {
        grid = new MGinterfaces.EGridType[rows, columns];
    }

    int MGinterfaces.IMaze.Width
    {
        get
        {
            return grid.GetUpperBound(1) + 1;
        }
    }
    int MGinterfaces.IMaze.Height
    {
        get
        {
            return grid.GetUpperBound(0) + 1;
        }
    }

    public void SetType(int x, int y, MGinterfaces.EGridType type)
    {
        grid[y, x] = type;
    }
    MGinterfaces.EGridType MGinterfaces.IMaze.GetType(int x, int y)
    {
        return grid[y, x];
    }
    MGinterfaces.EGridType MGinterfaces.IMaze.GetType(Vector2 cell)
    {
        return grid[(int)(cell.y), (int)(cell.x)];
    }
    Vector2 MGinterfaces.IMaze.PlayerRespawn
    {
        get
        {
            return playerRespawn;
        }
        set
        {
            playerRespawn = value;
        }
    }
    List<Vector2> MGinterfaces.IMaze.GhostRespawn
    {
        get
        {
            return ghostRespawn;
        }
        set
        {
            ghostRespawn = value;
        }
    }
    Vector2 MGinterfaces.IMaze.Tunel1
    {
        get
        {
            return tunel1;
        }
        set
        {
            tunel1 = value;
        }
    }
    Vector2 MGinterfaces.IMaze.Tunel2
    {
        get
        {
            return tunel2;
        }
        set
        {
            tunel2 = value;
        }
    }
    Vector2[] MGinterfaces.IMaze.corners
    {
        get
        {
            return this.corners;
        }
        set
        {
            this.corners = value;
        }
    }
}

public class ConcreteBuilder : MGinterfaces.IMazeBuilder<ConcreteMaze>
{
    ConcreteMaze maze;
    List<string> lines;
    MGinterfaces.IFigureBuilder<MGinterfaces.IFigure> figureBuilder;
    public ConcreteBuilder(List<string> file, MGinterfaces.IFigureBuilder<MGinterfaces.IFigure> figures)
    {
        lines = file;
        figureBuilder = figures;
        maze = new ConcreteMaze(file[0].Length, file.Count);
    }

    void MGinterfaces.IMazeBuilder<ConcreteMaze>.BuildDots()
    {
        for (int i = 0; i < lines.Count; i++)
        {
            MGinterfaces.EGridType[] digits = Array.ConvertAll(lines[i].ToCharArray(), c => (MGinterfaces.EGridType)Char.GetNumericValue(c));
            for (int j = 0; j < lines[0].Length; j++)
            {
                if (((MGinterfaces.IMaze)maze).GetType(j, i) == MGinterfaces.EGridType.None)
                {
                    if (digits[j] == MGinterfaces.EGridType.Dot)
                        maze.SetType(j, i, MGinterfaces.EGridType.Dot);
                    else
                    {
                        if (digits[j] == MGinterfaces.EGridType.Energizer)
                            maze.SetType(j, i, MGinterfaces.EGridType.Energizer);
                    }
                }
            }
        }
    }

    void MGinterfaces.IMazeBuilder<ConcreteMaze>.BuildRoom()
    {
        MGinterfaces.IMaze m = maze;
        m.GhostRespawn = new List<Vector2>();
        for (int i = 0; i < lines.Count; i++)
        {
            MGinterfaces.EGridType[] digits = Array.ConvertAll(lines[i].ToCharArray(), c => (MGinterfaces.EGridType)Char.GetNumericValue(c));
            for (int j = 0; j < lines[0].Length; j++)
            {
                if (digits[j] == MGinterfaces.EGridType.Room)
                {
                    maze.SetType(j, i, MGinterfaces.EGridType.Room);
                    m.GhostRespawn.Add(new Vector2((float)j, (float)i));
                }
            }
        }
    }

    void MGinterfaces.IMazeBuilder<ConcreteMaze>.BuildRespawns()
    {
        MGinterfaces.IMaze m = maze;
        m.PlayerRespawn = Vector2.zero;
        for (int i = 0; i < lines.Count; i++)
        {
            MGinterfaces.EGridType[] digits = Array.ConvertAll(lines[i].ToCharArray(), c => (MGinterfaces.EGridType)Char.GetNumericValue(c));
            for (int j = 0; j < lines[0].Length; j++)
            {
                if (digits[j] == MGinterfaces.EGridType.PlayerRespawn)
                {
                    maze.SetType(j, i, MGinterfaces.EGridType.PlayerRespawn);
                    m.PlayerRespawn = new Vector2((float)j, (float)i);
                }
                    
            }
        }
    }

    void MGinterfaces.IMazeBuilder<ConcreteMaze>.BuildTunnel()
    {
        MGinterfaces.IMaze m = maze;
        m.Tunel1 = Vector2.zero;
        m.Tunel2 = Vector2.zero;
        for (int i = 0; i < lines.Count; i++)
        {
            MGinterfaces.EGridType[] digits = Array.ConvertAll(lines[i].ToCharArray(), c => (MGinterfaces.EGridType)Char.GetNumericValue(c));
            for (int j = 0; j < lines[0].Length; j++)
            {
                if (digits[j] == MGinterfaces.EGridType.TunnelPoint)
                {
                    maze.SetType(j, i, MGinterfaces.EGridType.TunnelPoint);
                    if (m.Tunel1 == Vector2.zero)
                        m.Tunel1 = new Vector2((float)j, (float)i);
                    else
                        m.Tunel2 = new Vector2((float)j, (float)i);
                }
            }
        }
    }

    void MGinterfaces.IMazeBuilder<ConcreteMaze>.BuildWalls()
    {
        for(int i = 0; i < lines.Count; i++)
        {
            MGinterfaces.EGridType[] digits = Array.ConvertAll(lines[i].ToCharArray(), c => (MGinterfaces.EGridType)Char.GetNumericValue(c));
            for (int j = 0; j < lines[0].Length; j++)
            {
                if (digits[j] == MGinterfaces.EGridType.Wall)
                    maze.SetType(j, i, MGinterfaces.EGridType.Wall);
            }
        }
    }

    bool isBadCell(int x, int y)
    {
        MGinterfaces.IMaze m = maze;
        return ((m.GetType(x, y) == MGinterfaces.EGridType.Wall) || (m.GetType(x, y) == MGinterfaces.EGridType.Room));
    }

    bool isCurrentFigureCorrect(MGinterfaces.IFigure figure, int x, int y)
    {
        MGinterfaces.IMaze m = maze;
        MGinterfaces.EGridType[,] data = figure.GetData();
        for (int j = 0; j <= data.GetUpperBound(0); j++)
        {
            for (int i = 0; i <= data.GetUpperBound(1); i++)
            {
                if (m.GetType((x + i), (y + j)) != MGinterfaces.EGridType.Dot)
                    return false;
                bool left = false, right = false, up = false, down = false;
                if (data[j, i] != MGinterfaces.EGridType.Wall)
                    continue;
                if ((i == data.GetUpperBound(1)) || (data[j, i + 1] != MGinterfaces.EGridType.Wall))
                    right = true;
                if ((i == 0) || (data[j, i - 1] != MGinterfaces.EGridType.Wall))
                    left = true;
                if ((j == 0) || (data[j - 1, i] != MGinterfaces.EGridType.Wall))
                    up = true;
                if ((j == data.GetUpperBound(0)) || (data[j + 1, i] != MGinterfaces.EGridType.Wall))
                    down = true;
                if (up && isBadCell((x + i), (y + j - 1)))
                    return false;
                if (down && isBadCell((x + i), (y + j + 1)))
                    return false;
                if (left && isBadCell((x + i - 1), (y + j)))
                    return false;
                if (right && isBadCell((x + i + 1), (y + j)))
                    return false;
                if (up && left && isBadCell((x + i - 1), (y + j - 1)))
                    return false;
                if (up && right && isBadCell((x + i + 1), (y + j - 1)))
                    return false;
                if (down && left && isBadCell((x + i - 1), (y + j + 1)))
                    return false;
                if (down && right && isBadCell((x + i + 1), (y + j + 1)))
                    return false;
            }
        }
        return true;
    }

    bool isFigureCorrect(MGinterfaces.IFigure figure, int x, int y)
    {
        MGinterfaces.IMaze m = maze;
        for(int rotates = 0; rotates < 4; rotates++)
        {
            if (isCurrentFigureCorrect(figure, x, y))
                return true;
            figure.Rotate();
        }
        return false;
    }

    void copyFigure(MGinterfaces.IFigure figure, int x, int y)
    {
        MGinterfaces.EGridType[,] data = figure.GetData();
        for (int j = 0; j <= data.GetUpperBound(0); j++)
        {
            for (int i = 0; i <= data.GetUpperBound(1); i++)
            {
                if (data[j, i] == MGinterfaces.EGridType.Wall)
                    maze.SetType(x + i, y + j, MGinterfaces.EGridType.Wall);
            }
        }
    }

    void MGinterfaces.IMazeBuilder<ConcreteMaze>.BuildEnvironment()
    {
        for (int i = 2; i < (lines.Count - 2); i++)
        {
            for (int j = 2; j < (lines[0].Length - 2); j++)
            {
                if (((MGinterfaces.IMaze)maze).GetType(j, i) != MGinterfaces.EGridType.Dot)
                    continue;
                int k, z;
                for(k = i - 1; k < i + 2; k++)
                {
                    for (z = j - 1; z < j + 2; z++)
                    {
                        if ((((MGinterfaces.IMaze)maze).GetType(z, k) != MGinterfaces.EGridType.PlayerRespawn) && (((MGinterfaces.IMaze)maze).GetType(z, k) != MGinterfaces.EGridType.Dot))
                            break;
                    }
                    if (z != j + 2)
                        break;
                }
                if (k != i + 2)
                    continue;
                MGinterfaces.IFigure figure = figureBuilder.GetFigure();
                UInt32 counter = 0;
                while(!isFigureCorrect(figure, j, i))
                {
                    figure = figureBuilder.GetFigure();
                    counter++;
                    if (counter >= 10000)
                        break;
                }
                if (counter < 10000)
                {
                    copyFigure(figure, j, i);
                }
            }
        }
    }

    ConcreteMaze MGinterfaces.IMazeBuilder<ConcreteMaze>.GetMaze()
    {
        return maze;
    }
}

public class ConcreteMazeGenerator : MazeGenerator<ConcreteMaze>
{
    public ConcreteMazeGenerator(MGinterfaces.IMazeBuilder<ConcreteMaze> builder) : base(builder)
    {}
}
