﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ConcreteFigure : MGinterfaces.IFigure
{
    private MGinterfaces.EGridType[,] data;
    public MGinterfaces.EGridType[,] Data
    {
        get
        {
            return data;
        }
        set
        {
            data = value;
        }
    }

    MGinterfaces.EGridType[,] MGinterfaces.IFigure.GetData()
    {
        return data;
    }

    void MGinterfaces.IFigure.Rotate()
    {
        MGinterfaces.EGridType[,] newData = new MGinterfaces.EGridType[(data.GetUpperBound(1) + 1), (data.GetUpperBound(0) + 1)];
        for(int i = 0; i <= data.GetUpperBound(0); i++)
        {
            for (int j = 0; j <= data.GetUpperBound(1); j++)
            {
                newData[j, i] = data[i, j];
            }
        }
        this.Data = newData;
    }
}

public class ConcreteFigureBuilder : MGinterfaces.IFigureBuilder<MGinterfaces.IFigure>
{
    public ConcreteFigureBuilder()
    {}
    MGinterfaces.IFigure MGinterfaces.IFigureBuilder<MGinterfaces.IFigure>.GetFigure()
    {
        ConcreteFigure figure = new ConcreteFigure();
        int width = global.random.Next(1, 4);
        int haveAcorner = global.random.Next(1, 100);
        if (haveAcorner > 50)
            haveAcorner = 1;
        else
            haveAcorner = 0;
        int heigth = (haveAcorner == 0) ? 1 : (global.random.Next(2, 4));
        int cornerPosition = (haveAcorner == 0) ? 0 : (global.random.Next(0, (width - 1)));
        figure.Data = new MGinterfaces.EGridType[heigth, width];
        for(int i = 0; i < width; i++)
        {
            figure.Data[0, i] = MGinterfaces.EGridType.Wall;
            if((haveAcorner == 1) && (i == cornerPosition))
            {
                for(int j = 1; j < heigth; j++)
                {
                    figure.Data[j, i] = MGinterfaces.EGridType.Wall;
                }
            }
        }
        return figure;
    }
}
