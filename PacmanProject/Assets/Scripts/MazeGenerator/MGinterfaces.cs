﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MGinterfaces
{
    public enum EGridType
    {
        None,
        Wall,
        Dot,
        Energizer,
        Room,
        TunnelPoint,
        PlayerRespawn
    }
    public interface IMaze
    {
        int Width { get; }
        int Height { get; }
        Vector2[] corners { get; set; }
        EGridType GetType(int x, int y);
        EGridType GetType(Vector2 cell);
        Vector2 PlayerRespawn { get; set; }
        List<Vector2> GhostRespawn { get; set; }
        Vector2 Tunel1 { get; set; }
        Vector2 Tunel2 { get; set; }
    }

    public interface IMazeBuilder<MazeType>
        where MazeType : IMaze
    {
        void BuildWalls();
        void BuildTunnel();
        void BuildRoom();
        void BuildRespawns();
        void BuildDots();
        void BuildEnvironment();
        MazeType GetMaze();
    }

    public interface IFigure
    {
        EGridType[,] GetData();
        void Rotate();
    }

    public interface IFigureBuilder<T>
        where T: IFigure
    {
        T GetFigure();
    }
}

public abstract class MazeGenerator<MazeType>
    where MazeType : MGinterfaces.IMaze
{
    MGinterfaces.IMazeBuilder<MazeType> mazeBuilder;
    public MazeGenerator(MGinterfaces.IMazeBuilder<MazeType> builder)
    {
        mazeBuilder = builder;
    }
    public MazeType Construct()
    {
        mazeBuilder.BuildWalls();
        mazeBuilder.BuildTunnel();
        mazeBuilder.BuildRoom();
        mazeBuilder.BuildRespawns();
        mazeBuilder.BuildDots();
        mazeBuilder.BuildEnvironment();
        return mazeBuilder.GetMaze();
    }
}