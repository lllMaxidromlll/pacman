﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PulseGhostLight : MonoBehaviour
{
    public float MinIntensity = 0.3F;
    public float MaxIntensity = 1.0F;

    IStateMachine state = null;
    Light ghostLight = null;
    Coroutine pulseCoroutine = null;

    void Start ()
    {
        StartCoroutine(CheckCoroutine());
	}

    private void setIntensity(float intensity)
    {
       ghostLight.intensity = intensity;
    }

    IEnumerator GetLightCoroutine()
    {
        while(ghostLight == null)
        {
            yield return new WaitForSeconds(0.5F);
            List<Light> list = transform.GetComponentsInChildren<Light>().ToList();
            if (list.Count > 0)
                ghostLight = list.First();
        }
    }

    IEnumerator GetStateCoroutine()
    {
        while (state == null)
        {
            yield return new WaitForSeconds(0.5F);
            state = GetComponent<IStateMachine>();
        }
    }

    IEnumerator CheckCoroutine()
    {
        yield return StartCoroutine(GetLightCoroutine());
        yield return StartCoroutine(GetStateCoroutine());
        while (true)
        {
            yield return new WaitWhile(() => state.GetState() != State.Fear);
            pulseCoroutine = StartCoroutine(PulseCoroutine());
            yield return new WaitWhile(() => state.GetState() == State.Fear);
            StopCoroutine(pulseCoroutine);
            pulseCoroutine = null;
            setIntensity((MaxIntensity + MinIntensity) / 2);
        }
    }

    IEnumerator PulseCoroutine()
    {
        float intensity = MinIntensity;
        while(true)
        {
            while (intensity < MaxIntensity)
            {
                intensity += 0.05F;
                setIntensity(intensity);
                yield return new WaitForFixedUpdate();
            }
            while (intensity > MinIntensity)
            {
                intensity -= 0.05F;
                setIntensity(intensity);
                yield return new WaitForFixedUpdate();
            }
        }
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
