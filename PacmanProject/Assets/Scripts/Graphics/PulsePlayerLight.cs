﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulsePlayerLight : MonoBehaviour
{
    public float intensityMin = 0.3F;
    public float intensityMax = 0.8F;
         
    void Start ()
    {
        StartCoroutine(LightCoroutine());
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    IEnumerator LightCoroutine()
    {
        float intensity;
        while(true)
        {
            intensity = intensityMin;
            for (; intensity < intensityMax; intensity += 0.01F)
            {
                GetComponent<Light>().intensity = intensity;
                yield return new WaitForFixedUpdate();
            }
            for (; intensity > intensityMin; intensity -= 0.01F)
            {
                GetComponent<Light>().intensity = intensity;
                yield return new WaitForFixedUpdate();
            }
        }
    }
}
