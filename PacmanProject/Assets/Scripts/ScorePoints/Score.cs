﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour
{
    public uint scoreValue = 0;
    private uint stackPoints = 200;
    Rect drawRectangle; 


    void Start ()
    {
        drawRectangle = new Rect((Screen.width / 2 - 100), 15, 200, 30);
        global.DotEatEvent += Global_DotEatEvent;
        global.EnergizerEatEvent += Global_EnergizerEatEvent;
        global.FruitEatEvent += Global_FruitEatEvent;
        global.GhostDiedEvent += Global_GhostDiedEvent;
    }

    public void SetRect(Rect rect)
    {
        drawRectangle = rect;
    }

    private void OnDestroy()
    {
        global.DotEatEvent -= Global_DotEatEvent;
        global.EnergizerEatEvent -= Global_EnergizerEatEvent;
        global.FruitEatEvent -= Global_FruitEatEvent;
        global.GhostDiedEvent -= Global_GhostDiedEvent;
    }

    private void Global_GhostDiedEvent(GameObject ghost)
    {
        scoreValue += stackPoints;
        stackPoints *= 2;
    }

    private void Global_EnergizerEatEvent()
    {
        scoreValue += 50;
        stackPoints = 200;
    }

    private void Global_DotEatEvent()
    {
        scoreValue += 10;
    }

    private void Global_FruitEatEvent()
    {
        scoreValue += 100;
    }

    private void OnGUI()
    {
        GUI.Label(drawRectangle, "Score = " + scoreValue.ToString());
    }
}
