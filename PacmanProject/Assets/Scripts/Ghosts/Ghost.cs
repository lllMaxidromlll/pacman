﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Ghost : MonoBehaviour 
{
    public Dots DotsToStart
    {
        set
        {
            gameObject.GetComponent<IStateMachine>().SetDotsLimit(value);
        }
    }

    IMovement move;
    IStateMachine state;
    IGhostBehavior behavior;
    State lastState;

    void Awake ()
    {
        move = gameObject.GetComponent<IMovement>();
        state = gameObject.GetComponent<IStateMachine>();
        behavior = gameObject.GetComponent<IGhostBehavior>();
        global.EnergizerEatEvent += Global_EnergizerEatEvent;
        lastState = state.GetState();
    }

    private void OnDestroy()
    {
        global.EnergizerEatEvent -= Global_EnergizerEatEvent;
    }

    private void Global_EnergizerEatEvent()
    {
        StartCoroutine(ChangeFearDirection(0));
    }

    void moveByTarget()
    {
        List<Vector3> path = global.Path.GetDirection(transform.position, behavior.GetTarget(state.GetState()));
        if(path.Count > 0)
            if ((path.First() - transform.position).magnitude > 0.1F)
            {
                move.Move(path.First());
            }
    }

    IEnumerator ChangeFearDirection(float time)
    {
        yield return new WaitForFixedUpdate();
        moveByTarget();
    }

    void Update ()
    {
        State newState = state.GetState();
        if (lastState != newState)
        {
            if (newState == State.Fear)
                move.SetSpeed(global.Speed * 0.7F);
            else
            {
                if(lastState == State.Fear)
                    move.SetSpeed(global.Speed);
            }
            lastState = newState;
        }
        if (move.IsStopped())
            moveByTarget();
    }
    public static Ghost Create<MovementType, StateMachineType, BehaviorType>(GameObject ghostPrefab)
        where MovementType : MonoBehaviour, IMovement
        where StateMachineType : MonoBehaviour, IStateMachine
        where BehaviorType : MonoBehaviour, IGhostBehavior
    {
        ghostPrefab.AddComponent<MovementType>();
        ghostPrefab.AddComponent<StateMachineType>();
        ghostPrefab.AddComponent<BehaviorType>();
        ghostPrefab.AddComponent<PulseGhostLight>();
        return ghostPrefab.AddComponent<Ghost>();
    }
}
