﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClydeGhost : MonoBehaviour
{
    void Awake()
    {
        Ghost.Create<Movement, StateMachine, ClydeBehavior>(gameObject).DotsToStart = Dots.Eat90Dots;
        Destroy(this);
    }
}
