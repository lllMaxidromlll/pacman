﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinkyGhost : MonoBehaviour
{
    void Awake()
    {
        Ghost.Create<Movement, StateMachine, PinkyBehavior>(gameObject).DotsToStart = Dots.Eat30Dots;
        Destroy(this);
    }
}
