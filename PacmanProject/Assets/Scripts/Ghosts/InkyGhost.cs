﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InkyGhost : MonoBehaviour
{
    void Awake()
    {
        Ghost.Create<Movement, StateMachine, InkyBehavior>(gameObject).DotsToStart = Dots.Eat60Dots;
        Destroy(this);
    }
}
