﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGhostBehavior
{
    Vector3 GetTarget(State state);
}

public abstract class AbstractBehavior : MonoBehaviour, IGhostBehavior
{
    public virtual Vector3 GetTarget(State state)
    {
        if (state == State.Idle)
            return transform.position;
        Vector2 target = Vector2.zero;
        target.x = global.random.Next(2, global.Maze.Width);
        target.y = global.random.Next(2, global.Maze.Height);
        return target.ToVector3();
    }
}

public class BlinkyBehavior : AbstractBehavior
{
    public override Vector3 GetTarget(State state)
    {
        switch(state)
        {
            case State.Chase:
                return global.PlayerTransform.position;
            case State.Scatter:
                return (global.DotsLeft > 30) ? global.Maze.corners[0].ToVector3() : global.PlayerTransform.position;
        }
        return base.GetTarget(state);
    }
}
public class PinkyBehavior : AbstractBehavior
{
    public override Vector3 GetTarget(State state)
    {
        switch (state)
        {
            case State.Chase:
                {
                    Vector3 directionVector = global.PlayerTransform.TransformVector(Vector3.forward).normalized;
                    Vector3 vector = global.PlayerTransform.position;
                    Vector3 temporaryVector = vector + directionVector;
                    for (uint i = 0; i < 4; i++)
                    {
                        if (global.Path.CanMove(temporaryVector))
                            vector = temporaryVector;
                        temporaryVector += directionVector;
                    }
                    return vector;
                }
            case State.Scatter:
                return global.Maze.corners[1].ToVector3();
        }
        return base.GetTarget(state);
    }
}
public class InkyBehavior : AbstractBehavior
{
    Transform blinky = null;
    public void SetBlinkyTransform(Transform blinkyTransform)
    {
        blinky = blinkyTransform;
    }
    public override Vector3 GetTarget(State state)
    {
        switch (state)
        {
            case State.Chase:
                {
                    if (blinky == null)
                        break;
                    Vector3 target = blinky.position + (global.PlayerTransform.TransformPoint(global.PlayerTransform.position + Vector3.forward * 2) - blinky.position) * 2;
                    int magnitude = (int)((target - blinky.position).magnitude);
                    Vector3 directionVector = (target - blinky.position).normalized;
                    Vector3 temporaryVector = blinky.position + directionVector * magnitude;
                    while (magnitude-- > 0)
                    {
                        if (global.Path.CanMove(temporaryVector))
                            return temporaryVector;
                        temporaryVector -= directionVector;
                    }
                    return transform.position;
                }
            case State.Scatter:
                return global.Maze.corners[2].ToVector3();
        }
        return base.GetTarget(state);
    }
}
public class ClydeBehavior : AbstractBehavior
{
    public override Vector3 GetTarget(State state)
    {
        switch (state)
        {
            case State.Chase:
                {
                    if((transform.position - global.PlayerTransform.position).magnitude < 8)
                        return global.Maze.corners[3].ToVector3();
                    return global.PlayerTransform.position;
                }
            case State.Scatter:
                return global.Maze.corners[3].ToVector3();
        }
        return base.GetTarget(state);
    }
}