﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum State
{
    Idle,
    Scatter,
    Chase,
    Fear
}

public enum Dots
{
    Eat0Dots,
    Eat10Dots = 10,
    Eat20Dots = 20,
    Eat30Dots = 30,
    Eat40Dots = 40,
    Eat50Dots = 50,
    Eat60Dots = 60,
    Eat70Dots = 70,
    Eat80Dots = 80,
    Eat90Dots = 90
}

public interface IStateMachine
{
    State GetState();
    void SetDotsLimit(Dots dots);
    void SetStartTime(int idleDuration);
}
