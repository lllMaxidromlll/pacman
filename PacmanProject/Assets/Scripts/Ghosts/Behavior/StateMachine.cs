﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public interface IState
{
    State State { get; }
    int MillisecondsDuration { get; set; }
    int MillisecondsLeft { get; }
}

public abstract class AbstractState : MonoBehaviour, IState
{
    Coroutine coroutine = null;
    DateTime startTime;
    bool subscribed = false;
    int millisecondsDuration = 4000;
    public int MillisecondsDuration
    {
        get
        {
            return millisecondsDuration;
        }
        set
        {
            millisecondsDuration = value;
            ResetTimer();
        }
    }
    public int MillisecondsLeft
    {
        get
        {
            int elapsedMs = (int)((DateTime.Now - startTime).TotalMilliseconds);
            return (elapsedMs > MillisecondsDuration) ? 0 : (MillisecondsDuration - elapsedMs);
        }
    }
    public static UInt32 scatterCounter = 4;

    protected virtual void Awake()
    {
        ResetTimer();
        startCoroutine();
        enableEventHandlers();
    }

    private void enableEventHandlers()
    {
        if (!subscribed)
        {
            global.EnergizerEatEvent += Global_EnergizerEatEvent;
            subscribed = true;
        }
    }

    private void disableEventHandlers()
    {
        if (subscribed)
        {
            subscribed = false;
            global.EnergizerEatEvent -= Global_EnergizerEatEvent;
        }
    }

    private void startCoroutine()
    {
        if (coroutine == null)
            coroutine = StartCoroutine(StateCoroutine());
    }

    private void stopCoroutine()
    {
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
            coroutine = null;
        }
    }

    protected virtual void OnEnable()
    {
        ResetTimer();
        enableEventHandlers();
    }

    protected virtual void OnDisable()
    {
        Stop();
    }

    protected virtual void OnDestroy()
    {
        Stop();
    }

    private void Global_EnergizerEatEvent()
    {
        FearStateChanged();
    }

    protected virtual void FearStateChanged()
    {
        Stop();
        MillisecondsDuration = MillisecondsLeft;
        gameObject.AddComponent<FearState>();
    }

    public void Pause()
    {
        stopCoroutine();
        MillisecondsDuration = MillisecondsLeft;
    }

    public void Stop()
    {
        stopCoroutine();
        disableEventHandlers();
    }

    public void Resume()
    {
        ResetTimer();
        startCoroutine();
        enableEventHandlers();
    }

    public void ResetTimer()
    {
        startTime = DateTime.Now;
    }

    IEnumerator StateCoroutine()
    {
        yield return new WaitWhile(() => MillisecondsLeft > 0);
        Stop();
        ChangeStateHandler();
        Destroy(this);
    }

    public abstract State State { get; }
    protected abstract void ChangeStateHandler();
}

public class IdleState : AbstractState
{
    public override State State
    {
        get
        {
            return State.Idle;
        }
    }
    private Dots dots;
    int dotsEaten;

    protected override void Awake()
    {
        base.Awake();
        dotsEaten = 0;
        dots = Dots.Eat90Dots;
        MillisecondsDuration = 4000;
        global.DotEatEvent += Global_DotEatEvent;
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        global.DotEatEvent += Global_DotEatEvent;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        global.DotEatEvent -= Global_DotEatEvent;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        global.DotEatEvent -= Global_DotEatEvent;
    }

    private void Global_DotEatEvent()
    {
        dotsEaten++;
        MillisecondsDuration = MillisecondsDuration;
    }

    protected override void ChangeStateHandler()
    {
        gameObject.AddComponent<ScatterState>();
    }
    public void SetDotsToStart(Dots dots)
    {
        this.dots = dots;
    }
    private void Update()
    {
        if(dotsEaten >= (int)dots)
            MillisecondsDuration = 0;
    }
}

public class ScatterState : AbstractState
{
    public override State State
    {
        get
        {
            return State.Scatter;
        }
    }

    void Start()
    {
        MillisecondsDuration = global.random.Next(17, 7000);
        if (scatterCounter > 0)
            scatterCounter--;
        else
            MillisecondsDuration = 0;
    }
    
    protected override void ChangeStateHandler()
    {
        gameObject.AddComponent<ChaseState>();
    }
}

public class ChaseState : AbstractState
{
    public override State State
    {
        get
        {
            return State.Chase;
        }
    }

    void Start()
    {
        MillisecondsDuration = global.random.Next(20000, 40000);
    }

    protected override void ChangeStateHandler()
    {
        gameObject.AddComponent<ScatterState>();
    }
}

public class FearState : AbstractState
{
    public override State State
    {
        get
        {
            return State.Fear;
        }
    }

    void Start()
    {
        MillisecondsDuration = 5000;
    }
    
    protected override void ChangeStateHandler()
    {
        List<AbstractState> list = GetComponents<AbstractState>().ToList();
        list.ForEach(s => s.Resume());
    }
    protected override void FearStateChanged()
    {
        MillisecondsDuration = 5000;
    }
}

public class StateMachine : MonoBehaviour, IStateMachine
{
    Dots dots = Dots.Eat90Dots;
    int startTime = 4000;
    State IStateMachine.GetState()
    {
        List<IState> list = gameObject.GetComponents<IState>().ToList();
        if (list.Count(s => s.State == State.Idle) > 0)
            return State.Idle;
        if (list.Count(s => s.State == State.Fear) > 0)
            return State.Fear;
        return list.First().State;
    }
    void IStateMachine.SetDotsLimit(Dots dots)
    {
        this.dots = dots;
        gameObject.GetComponents<IdleState>().ToList().ForEach(s => s.SetDotsToStart(dots));
    }
    void IStateMachine.SetStartTime(int idleDuration)
    {
        this.startTime = idleDuration;
        gameObject.GetComponents<IdleState>().ToList().ForEach(s => s.MillisecondsDuration = idleDuration);
    }
    bool needReset = false;
    bool subscribed = false;

    void Awake()
    {
        AbstractState.scatterCounter = 4;
        enableEventHandlers();
        IdleState idle = gameObject.AddComponent<IdleState>();
        idle.Pause();
    }

    void OnEnable()
    {
        enableEventHandlers();
    }

    void OnDisable()
    {
        disableEventHandlers();
    }

    void OnDestroy()
    {
        disableEventHandlers();
    }

    private void enableEventHandlers()
    {
        if(!subscribed)
        {
            subscribed = true;
            global.GhostDiedEvent += Global_GhostDiedEvent;
            global.PlayerDiedEvent += Global_PlayerDiedEvent;
            global.StartEvent += Global_StartEvent;
            global.PauseEvent += Global_PauseEvent;
        }
    }

    private void disableEventHandlers()
    {
        if(subscribed)
        {
            subscribed = false;
            global.GhostDiedEvent -= Global_GhostDiedEvent;
            global.PlayerDiedEvent -= Global_PlayerDiedEvent;
            global.StartEvent -= Global_StartEvent;
            global.PauseEvent -= Global_PauseEvent;
        }
    }

    private void ResetState()
    {
        gameObject.GetComponents<AbstractState>().ToList().ForEach(s => MonoBehaviour.DestroyImmediate(s));
        IdleState newState = gameObject.AddComponent<IdleState>();
        newState.SetDotsToStart(dots);
        newState.MillisecondsDuration = startTime;
    }

    private void Global_GhostDiedEvent(GameObject obj)
    {
        if (obj.Equals(gameObject))
            needReset = true;
    }

    private void Global_PlayerDiedEvent()
    {
        AbstractState.scatterCounter = 4;
        if (dots > Dots.Eat0Dots)
            dots--;
        needReset = true;
    }

    private void Global_PauseEvent()
    {
        GetComponents<AbstractState>().ToList().ForEach(s => s.Pause());
    }

    private void Global_StartEvent()
    {
        List<AbstractState> states = gameObject.GetComponents<FearState>().ToList().ConvertAll(f => f as AbstractState);
        if (states.Count == 0)
            states = gameObject.GetComponents<AbstractState>().ToList();
        states.ForEach(s => s.Resume());
    }

    void Update()
    {
        if(needReset)
        {
            needReset = false;
            ResetState();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if ((this as IStateMachine).GetState() != State.Fear)
                global.PlayerDied();
            else
                global.GhostDied(gameObject);
        }
    }
}
