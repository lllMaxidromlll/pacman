﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkyGhost : MonoBehaviour
{
	void Awake ()
    {
        Ghost.Create<Movement, StateMachine, BlinkyBehavior>(gameObject).DotsToStart = Dots.Eat0Dots;
        Destroy(this);
    }
}
