﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : GameStateBehavior, IMovement
{
    Direction direction;
    Vector3 targetPosition;
    Vector3 startPosition;
    float speed;

    public bool IsStopped()
    {
        if ((targetPosition - transform.position).magnitude > 0.01F)
            return false; // Если не передвинулсь на клетку то продолжаем.
        if (global.Maze.GetType(transform.position.ToVector2()) == MGinterfaces.EGridType.Room)
            return true;  // Если мы в комнатке, то передвигаемся не по прямым а по клеткам, поэтому нужно получить новую клетку.
        if (!global.Path.CanMove(transform.position, direction))
            return true;  // Если мы двигались по прямой и уперлись в стенку.
        targetPosition = targetPosition.ChangePosition(direction); // Двигаемся дальше.
        return false;
    }

    void IMovement.Move(Vector3 targetPosition)
    {
        this.targetPosition = targetPosition;
        direction = (targetPosition - transform.position).normalized.ToVector2().ToDirection();
    }

    void IMovement.SetSpeed(float speed)
    {
        this.speed = speed;
    }

    protected override void Start ()
    {
        base.Start();
        speed = global.Speed;
        startPosition = targetPosition = transform.position;
        transform.LookAt((transform.position + Vector3.up), global.camera.TransformDirection(Vector3.back));
        global.GhostDiedEvent += Global_GhostDiedEvent;
        global.PlayerDiedEvent += Global_PlayerDiedEvent;
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
        global.GhostDiedEvent -= Global_GhostDiedEvent;
        global.PlayerDiedEvent -= Global_PlayerDiedEvent;
    }

    void resetPosition()
    {
        transform.position = targetPosition = startPosition;
        transform.Translate(Vector3.zero);
    }

    private void Global_PlayerDiedEvent()
    {
        StartCoroutine(ResetPositionEnumerator());
    }

    private void Global_GhostDiedEvent(GameObject obj)
    {
        if (obj.Equals(gameObject))
            StartCoroutine(ResetPositionEnumerator());
    }

    IEnumerator ResetPositionEnumerator()
    {
        yield return new WaitForEndOfFrame();
        transform.position = targetPosition = startPosition;
        transform.Translate(Vector3.zero);
    }

    protected override void UpdateImplementation()
    {
        if (!IsStopped())
        {
            transform.LookAt(targetPosition, transform.up);
            if ((targetPosition - transform.position).magnitude > Time.deltaTime * speed)
                transform.Translate((targetPosition - transform.position).normalized * Time.deltaTime * speed, Space.World);
            else
                transform.Translate(targetPosition - transform.position, Space.World);
        }
    }
}
