﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction
{
    Left,
    Right,
    Up,
    Down
}

public interface IPath
{
    bool CanMove(Vector3 from);
    bool CanMove(Vector3 from, Direction to);
    List<Vector3> GetDirection(Vector3 position, Vector3 target);
}
