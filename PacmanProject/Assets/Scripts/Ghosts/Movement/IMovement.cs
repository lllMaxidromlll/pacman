﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMovement
{
    void Move(Vector3 targetPosition);
    void SetSpeed(float speed);
    bool IsStopped();
}
