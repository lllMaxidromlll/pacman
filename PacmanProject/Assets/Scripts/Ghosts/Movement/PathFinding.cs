﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

static class VectorsExtensions
{
    public static Vector2 ToVector2(this Vector3 vector)
    {
        Vector2 resultVector = Vector2.zero;
        resultVector.x = Mathf.Round(vector.x);
        resultVector.y = Mathf.Round(vector.y * (-1));
        return resultVector;
    }
    public static Vector3 ToVector3(this Vector2 vector)
    {
        Vector3 resultVector = Vector3.zero;
        resultVector.x = vector.x;
        resultVector.y = vector.y * (-1);
        return resultVector;
    }
    public static Vector3 ChangePosition(this Vector3 vector, Direction direction)
    {
        return vector.ToVector2().ChangePosition(direction).ToVector3();
    }
    public static Direction ToDirection(this Vector2 vector)
    {
        if (Mathf.Abs(vector.y) > Mathf.Abs(vector.x))
        {
            if (vector.y >= 0)
                return Direction.Up;
            return Direction.Down;
        }
        if (vector.x >= 0)
            return Direction.Right;
        return Direction.Left;
    }
    public static Vector2 ChangePosition(this Vector2 vector, Direction direction)
    {
        Vector2 result = Vector2.zero;
        switch (direction)
        {
            case Direction.Down:
                result = vector + Vector2.down;
                break;
            case Direction.Left:
                result = vector + Vector2.left;
                break;
            case Direction.Right:
                result = vector + Vector2.right;
                break;
            case Direction.Up:
                result = vector + Vector2.up;
                break;
        }
        if(result == global.Maze.Tunel1)
            result = global.Maze.Tunel2 + (result - vector);
        else
        {
            if (result == global.Maze.Tunel2)
                result = global.Maze.Tunel1 + (result - vector);
        }
        return result;
    }
}

public class PathFinding : IPath
{
    MGinterfaces.IMaze maze;

    class Path : List<Vector2>
    {
        public Path(){}
    }

    public PathFinding(MGinterfaces.IMaze maze)
    {
        this.maze = maze;
    }

    bool isRoadCell(Vector2 cell, bool checkRoom = true)
    {
        if ((cell.x < 0) || (cell.y < 0))
            return false;
        if ((cell.x >= maze.Width) || (cell.y >= maze.Height))
            return false;
        if (maze.GetType((int)(cell.x), (int)(cell.y)) == MGinterfaces.EGridType.Wall)
            return false;
        if(checkRoom)
        {
            if (maze.GetType((int)(cell.x), (int)(cell.y)) == MGinterfaces.EGridType.Room)
                return false;
        }
        return true;
    }

    bool isRoad(Vector2 from, Vector2 to)
    {
        return isRoadCell(to, (maze.GetType((int)(from.x), (int)(from.y)) != MGinterfaces.EGridType.Room));
    }

    bool CanMove(Vector2 from, Direction to)
    {
        return isRoad(from, from.ChangePosition(to));
    }

    bool IPath.CanMove(Vector3 to)
    {
        return isRoadCell(to.ToVector2());
    }

    bool IPath.CanMove(Vector3 from, Direction to)
    {
        return CanMove(from.ToVector2(), to);
    }

    List<Vector2> findNearestLines(Vector2 position, Vector2 target, ref List<Vector2> busy)
    {
        List<Vector2> result = new List<Vector2>();
        List<Direction> directions = Enum.GetNames(typeof(Direction)).ToList().ConvertAll<Direction>(s => (Direction)Enum.Parse(typeof(Direction), s));
        foreach (Direction d in directions)
        {
            Vector2 newPosition = position;
            if (!CanMove(newPosition, d))
                continue;
            while (CanMove(newPosition, d))
            {
                newPosition = newPosition.ChangePosition(d);
                if (newPosition == target)
                    break;
            }
            if (newPosition == target)
            {
                result.Add(newPosition);
                continue;
            }
            if (busy.Contains(newPosition))
                continue;
            busy.Add(newPosition);
            result.Add(newPosition);
        }
        return result;
    }

    List<Path> findLinePaths(Vector2 position, Vector2 target)
    {
        List<Vector2> busy = new List<Vector2>();
        List<Path> paths = new List<Path>();
        Dictionary<Path, float> distance = new Dictionary<Path, float>();
        List<Path> temporaryPaths = new List<Path>();
        temporaryPaths.Add(new Path());
        temporaryPaths.Last().Add(position);
        while (temporaryPaths.Count > 0)
        {
            List<Path> nextPaths = new List<Path>();
            for (int i = 0; i < temporaryPaths.Count; i++)
            {
                List<Vector2> lines = findNearestLines(temporaryPaths[i].Last(), target, ref busy);
                if(lines.Count == 0)
                {
                    paths.Add(temporaryPaths[i]);
                    float minDistance = (target - paths.Last()[0]).magnitude;
                    paths.Last().ForEach(p => minDistance = ((target - p).magnitude < minDistance) ? (target - p).magnitude : minDistance);
                    distance.Add(paths.Last(), minDistance);
                    temporaryPaths.RemoveAt(i);
                    i--;
                    continue;
                }
                if(lines.Contains(target))
                {
                    paths = new List<Path>();
                    paths.Add(temporaryPaths[i]);
                    paths.Last().Add(target);
                    lines.Remove(target);
                    return paths;
                }
                if (temporaryPaths[i].Count > 1)
                    lines.RemoveAll(l => (temporaryPaths[i].Last() - temporaryPaths[i].ElementAt(temporaryPaths[i].Count - 2)).ToDirection() == ((temporaryPaths[i].Last() - l).ToDirection()));
                for(int j = 0; j < lines.Count; j++)
                {
                    Path p = new Path();
                    p.AddRange(temporaryPaths[i]);
                    p.Add(lines[j]);
                    nextPaths.Add(p);
                }
            }
            temporaryPaths = nextPaths;
        }
        paths = new List<Path>();
        paths.Add(distance.OrderByDescending(d => d.Value).Last().Key);
        return paths;
    }

    List<Vector2> findNearestPoints(Vector2 position, ref List<Vector2> busy)
    {
        List<Vector2> result = new List<Vector2>();
        List<Direction> directions = Enum.GetNames(typeof(Direction)).ToList().ConvertAll<Direction>(s => (Direction)Enum.Parse(typeof(Direction), s));
        foreach (Direction d in directions)
        {
            if (!CanMove(position, d))
                continue;
            Vector2 newPosition = position.ChangePosition(d);
            result.Add(newPosition);
            if (global.Maze.GetType(newPosition) != MGinterfaces.EGridType.Room)
            {
                result.Clear();
                result.Add(newPosition);
                break;
            }
            if (busy.Contains(newPosition))
                continue;
            busy.Add(newPosition);
            result.Add(newPosition);
        }
        return result;
    }

    List<Path> findExitFromRoom(Vector2 position)
    {
        List<Vector2> busy = new List<Vector2>();
        List<Path> temporaryPaths = new List<Path>();
        temporaryPaths.Add(new Path());
        temporaryPaths.Last().Add(position);
        while (temporaryPaths.Count > 0)
        {
            List<Path> nextPaths = new List<Path>();
            for (int i = 0; i < temporaryPaths.Count; i++)
            {
                List<Vector2> points = findNearestPoints(temporaryPaths[i].Last(), ref busy);
                if (points.Count == 0)
                {
                    temporaryPaths.RemoveAt(i);
                    i--;
                    continue;
                }
                if (points.Where(p => (global.Maze.GetType(p) != MGinterfaces.EGridType.Room)).Count() > 0)
                {
                    List<Path> paths = new List<Path>();
                    paths.Add(temporaryPaths[i]);
                    paths.Last().Add(points[0]);
                    return paths;
                }
                for (int j = 0; j < points.Count; j++)
                {
                    Path p = new Path();
                    p.AddRange(temporaryPaths[i]);
                    p.Add(points[j]);
                    nextPaths.Add(p);
                }
            }
            temporaryPaths = nextPaths;
        }
        return new List<Path>();
    }

    List<Vector3> IPath.GetDirection(Vector3 position, Vector3 target)
    {
        List<Vector3> result = new List<Vector3>();
        if ((position == target) || (!isRoadCell(target.ToVector2(), false)))
            result.Add(position);
        else
        {
            List<Path> paths;
            if (global.Maze.GetType(position.ToVector2()) == MGinterfaces.EGridType.Room)
                paths = findExitFromRoom(position.ToVector2());
            else
                paths = findLinePaths(position.ToVector2(), target.ToVector2());
            if (paths.Count == 0)
                result.Add(position);
            else
            {
                result = paths.First().Skip(1).ToList().ConvertAll(p => p.ToVector3());
            }
        }
        return result;
    }
}
