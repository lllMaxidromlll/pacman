﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

internal static class LevelBuilder
{
    static GameObject currentMaze = null;
    static void setActive(GameObject go, bool isActive)
    {
        go.SetActive(isActive);
        for (int i = 0; i < go.transform.childCount; i++)
        {
            go.transform.GetChild(i).gameObject.SetActive(isActive);
        }
    }
    static GameObject rebuildMaze()
    {
        GameObject go = null;
        if (currentMaze != null)
        {
            go = currentMaze.GetComponent<Maze>().Clone();
            setActive(go, true);
            global.Maze = go.GetComponent<Maze>();
            global.Path = new PathFinding((MGinterfaces.IMaze)(global.Maze));
        }
        return go;
    }
    static GameObject buildMaze(GameObject mazePrefab)
    {
        if (currentMaze != null)
            MonoBehaviour.Destroy(currentMaze);
        currentMaze = MonoBehaviour.Instantiate(mazePrefab, Vector3.zero, Quaternion.Euler(Vector3.zero));
        currentMaze.GetComponent<Maze>().Build();
        setActive(currentMaze, false);
        return rebuildMaze();
    }
    static GameObject buildPlayer(GameObject playerPrefab)
    {
        return MonoBehaviour.Instantiate(playerPrefab, new Vector2(global.Maze.PlayerRespawn.x, global.Maze.PlayerRespawn.y).ToVector3(), Quaternion.Euler(new Vector3(90.0F, 0, 0)));
    }
    static List<GameObject> buildGhosts(List<GameObject> ghostPrefabs)
    {
        int ghostCounter = 0;
        List<GameObject> result = ghostPrefabs.Where(g => (g != null)).ToList().ConvertAll(g => MonoBehaviour.Instantiate(g, (global.Maze.GhostRespawn[ghostCounter++]).ToVector3(), Quaternion.Euler(Vector3.zero)));
        for(int i = 0; i < result.Count; i++)
        {
            result[i].GetComponent<IStateMachine>().SetStartTime(i * 4000);
        }
        InkyBehavior inky = result.Where(g => (g.GetComponent<InkyBehavior>() != null)).ToList().ConvertAll(g => g.GetComponent<InkyBehavior>()).FirstOrDefault();
        if (inky != null)
        {
            BlinkyBehavior blinky = result.Where(g => (g.GetComponent<BlinkyBehavior>() != null)).ToList().ConvertAll(g => g.GetComponent<BlinkyBehavior>()).FirstOrDefault();
            if (blinky != null)
                inky.SetBlinkyTransform(blinky.transform);
        }
        return result;
    }

    public static GameObject Build(GameObject mazePrefab, GameObject playerPrefab, List<GameObject> ghostPrefabs)
    {
        if ((mazePrefab == null) || (playerPrefab == null))
            return null;
        GameObject emptyPrefab = GameObject.Find("empty");
        if(emptyPrefab == null)
        {
            emptyPrefab = Resources.Load<GameObject>("EmptyPrefab");
            emptyPrefab.name = "empty";
        }
        GameObject level = MonoBehaviour.Instantiate<GameObject>(emptyPrefab);
        level.name = "Level";
        buildMaze(mazePrefab).transform.SetParent(level.transform);
        buildPlayer(playerPrefab).transform.SetParent(level.transform);
        List<GameObject> ghosts = buildGhosts(ghostPrefabs);
        foreach (GameObject go in ghosts)
            go.transform.SetParent(level.transform);
        return level;
    }
    public static GameObject ReBuild(GameObject playerPrefab, List<GameObject> ghostPrefabs)
    {
        GameObject level = GameObject.Find("Level");
        if (level != null)
            MonoBehaviour.Destroy(level);
        GameObject emptyPrefab = GameObject.Find("empty");
        if (emptyPrefab == null)
        {
            emptyPrefab = Resources.Load<GameObject>("EmptyPrefab");
            emptyPrefab.name = "empty";
        }
        level = MonoBehaviour.Instantiate<GameObject>(emptyPrefab);
        level.name = "Level";
        rebuildMaze().transform.SetParent(level.transform);
        buildPlayer(playerPrefab).transform.SetParent(level.transform);
        List<GameObject> ghosts = buildGhosts(ghostPrefabs);
        foreach (GameObject go in ghosts)
            go.transform.SetParent(level.transform);
        return level;
    }
}

public class Game : MonoBehaviour
{
    public uint DotsLeft;
    public uint DotsEat;
    public GameObject MazePrefab;
    public GameObject PlayerPrefab;
    public GameObject FruitPrefab;
    public GameObject Fireworks;
    public List<GameObject> GhostPrefabs = new List<GameObject>();
    uint dotsEat;

    private void buildGui()
    {
        gameObject.AddComponent<SimpleGUI>();
    }

    private void Awake()
    {
        dotsEat = 0;
        global.NextMapEvent += Global_NextMapEvent;
        global.ResetEvent += Global_ResetEvent;
        global.DotEatEvent += Global_DotEatEvent;
        LevelBuilder.Build(MazePrefab, PlayerPrefab, GhostPrefabs);
        buildGui();
    }

    private void buildFruit()
    {
        int counter = 0;
        if(FruitPrefab != null)
        {
            while (counter++ < 10000)
            {
                int x = global.random.Next(1, global.Maze.Width - 2);
                int y = global.random.Next(1, global.Maze.Height - 2);
                Vector3 fruitPosition = new Vector2(x, y).ToVector3();
                if (global.Path.CanMove(fruitPosition))
                {
                    List<Direction> directions = Enum.GetNames(typeof(Direction)).ToList().ConvertAll<Direction>(s => (Direction)Enum.Parse(typeof(Direction), s));
                    bool inPoint = true;
                    foreach(Direction d in directions)
                    {
                        Vector3 direction = (fruitPosition.ChangePosition(d) - fruitPosition).normalized;
                        int layerMask = (1 << 9);
                        List<RaycastHit> list = Physics.RaycastAll(fruitPosition, direction, Mathf.Max(global.Maze.Width, global.Maze.Height), layerMask).ToList();
                        inPoint = (list.Count > 0) ? true : false;
                        if (!inPoint)
                            break;
                    }
                    if (inPoint)
                    {
                        MonoBehaviour.Instantiate(FruitPrefab, fruitPosition, Quaternion.Euler(Vector3.zero));
                        break;
                    }
                }
            }
        }
    }

    private void buildFireworks()
    {
        Vector3 position = new Vector2(global.Maze.Width / 2, 0).ToVector3();
        position.z += 15;
        if(Fireworks != null)
        {
            Transform t = MonoBehaviour.Instantiate(Fireworks, position, Quaternion.Euler(global.camera.TransformDirection(Vector3.back))).transform;
            t.LookAt(global.camera.position);
            MonoBehaviour.Destroy(t.gameObject, 15);
        }
    }

    private void Global_DotEatEvent()
    {
        dotsEat++;
        if ((dotsEat == 70) || (dotsEat == 170))
            buildFruit();
        if (global.DotsLeft == 0)
            buildFireworks();
    }

    private void OnDestroy()
    {
        global.NextMapEvent -= Global_NextMapEvent;
        global.ResetEvent -= Global_ResetEvent;
        global.DotEatEvent -= Global_DotEatEvent;
    }

    private void Global_ResetEvent()
    {
        global.DotsLeft = 0;
        dotsEat = 0;
        LevelBuilder.ReBuild(PlayerPrefab, GhostPrefabs);
    }

    private void Global_NextMapEvent()
    {
        GameObject go = GameObject.Find("Level");
        if (go != null)
            Destroy(go);
        global.DotsLeft = 0;
        dotsEat = 0;
        LevelBuilder.Build(MazePrefab, PlayerPrefab, GhostPrefabs);
    }

    private void Update()
    {
        DotsLeft = global.DotsLeft;
        DotsEat = this.dotsEat;
    }
}
