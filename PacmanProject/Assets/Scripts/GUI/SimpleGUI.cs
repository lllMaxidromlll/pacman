﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseGUI : MonoBehaviour
{
    private void OnGUI()
    {
        GuiHandler();
    }
    protected virtual void GuiHandler()
    {
        if (GUI.Button(new Rect(25, 25, 60, 30), "Quit"))
        {
            Application.Quit();
        }
    }
}

public class LevelSelectMenu : BaseGUI
{
    protected override void GuiHandler()
    {
        base.GuiHandler();
        if (GUI.Button(new Rect(25, 75, 60, 30), "Next"))
        {
            global.NextMap();
            gameObject.AddComponent<MainMenu>();
            Destroy(this);
        }
    }
}

public class MainMenu : LevelSelectMenu
{
    protected override void GuiHandler()
    {
        base.GuiHandler();
        if (GUI.Button(new Rect(25, 125, 60, 30), "Start"))
        {
            Score scoreSystem = gameObject.GetComponent<Score>();
            if (scoreSystem != null)
                Destroy(scoreSystem);
            scoreSystem = gameObject.AddComponent<Score>();
            scoreSystem.SetRect(               new Rect((Screen.width / 2 - 200), 15, 200, 30));
            GameInProcessMenu.LivesRectangle = new Rect((Screen.width / 2      ), 15, 200, 30);
            global.StartMap();
            gameObject.AddComponent<GameStartMenu>();
            Destroy(this);
        }
    }
}


public class StoppedMenu : LevelSelectMenu
{
    protected override void GuiHandler()
    {
        base.GuiHandler();
        if (GUI.Button(new Rect(25, 125, 60, 30), "Again"))
        {
            global.ResetMap();
            gameObject.AddComponent<MainMenu>();
            Destroy(this);
        }
    }
}

public class GameInProcessMenu : BaseGUI
{
    static protected int playerLives = 3;
    static public Rect LivesRectangle = new Rect(0, 0, 200, 30);
    protected override void GuiHandler()
    {
        base.GuiHandler();
        GUI.Label(LivesRectangle, "Lives: " + playerLives.ToString());
        if (GUI.Button(new Rect(25, 75, 60, 30), "Stop") || (global.DotsLeft == 0))
        {
            global.PauseMap();
            gameObject.AddComponent<StoppedMenu>();
            Destroy(this);
        }
    }
}

public class GameStartMenu : GameInProcessMenu
{
    bool playerDied;
    private void Awake()
    {
        playerDied = false;
        global.PlayerDiedEvent += Global_PlayerDiedEvent;
    }
    private void OnDestroy()
    {
        playerDied = false;
        global.PlayerDiedEvent -= Global_PlayerDiedEvent;
    }

    private void Global_PlayerDiedEvent()
    {
        playerDied = true;
    }

    protected override void GuiHandler()
    {
        base.GuiHandler();
        if (GUI.Button(new Rect(25, 125, 60, 30), "Pause") || playerDied)
        {
            if(playerDied)
            {
                playerDied = false;
                playerLives--;
            }
            global.PauseMap();
            if (playerLives > 0)
                gameObject.AddComponent<GamePauseMenu>();
            else
            {
                gameObject.AddComponent<StoppedMenu>();
                playerLives = 3;
            }
            Destroy(this);
        }
    }
}

public class GamePauseMenu : GameInProcessMenu
{
    protected override void GuiHandler()
    {
        base.GuiHandler();
        if (GUI.Button(new Rect(25, 125, 60, 30), "Resume"))
        {
            global.StartMap();
            gameObject.AddComponent<GameStartMenu>();
            Destroy(this);
        }
    }
}

public class SimpleGUI : MonoBehaviour
{
	void Start ()
    {
        gameObject.AddComponent<MainMenu>();
        Destroy(this);
	}
}
