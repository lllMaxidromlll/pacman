﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GameStateBehavior : MonoBehaviour
{
    bool gameStarted;
	protected virtual void Start ()
    {
        gameStarted = false;
        global.StartEvent += Global_StartEvent;
        global.PauseEvent += Global_PauseEvent;
    }

    protected virtual void OnEnable()
    {
        global.StartEvent += Global_StartEvent;
        global.PauseEvent += Global_PauseEvent;
    }

    protected virtual void OnDisable()
    {
        global.StartEvent -= Global_StartEvent;
        global.PauseEvent -= Global_PauseEvent;
    }

    protected virtual void OnDestroy()
    {
        global.StartEvent -= Global_StartEvent;
        global.PauseEvent -= Global_PauseEvent;
    }
    private void Global_PauseEvent()
    {
        gameStarted = false;
    }

    private void Global_StartEvent()
    {
        gameStarted = true;
    }
    void Update ()
    {
        if (gameStarted)
            UpdateImplementation();
    }
    protected abstract void UpdateImplementation();
}
