﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DotRotate : MonoBehaviour
{
    Vector3 rotationAngles;
	void Start ()
    {
        float x = (global.random.Next(1, 100) > 50) ? 1.0F : 0;
        float y = (global.random.Next(1, 100) > 50) ? 1.0F : 0;
        float z = (global.random.Next(1, 100) > 50) ? 1.0F : 0;
        rotationAngles = new Vector3(x, y, z);
    }
	void FixedUpdate ()
    {
        transform.Rotate(rotationAngles);
	}
}
