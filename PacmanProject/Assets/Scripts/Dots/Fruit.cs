﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fruit : GameStateBehavior
{
    float deadCounter;
    protected override void Start()
    {
        base.Start();
        deadCounter = 0;
        global.ResetEvent += Global_ResetEvent;
        global.NextMapEvent += Global_NextMapEvent;
    }

    private void Global_NextMapEvent()
    {
        Destroy(gameObject);
    }

    private void Global_ResetEvent()
    {
        Destroy(gameObject);
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        global.ResetEvent -= Global_ResetEvent;
        global.NextMapEvent -= Global_NextMapEvent;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            global.FruitEat();
            Destroy(gameObject);
        }
    }

    protected override void UpdateImplementation()
    {
        deadCounter += Time.deltaTime;
        if (deadCounter > 9.0F)
            Destroy(gameObject);
    }
}
