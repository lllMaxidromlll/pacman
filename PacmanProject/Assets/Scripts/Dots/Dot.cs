﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dot : MonoBehaviour
{
	void Start ()
    {
        global.DotsLeft++;
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            global.DotsLeft--;
            global.DotEat();
            MonoBehaviour.Destroy(GetComponent<Collider>());
            StartCoroutine(DestroyCorutine());
        }
    }
    IEnumerator DestroyCorutine()
    {
        yield return new WaitForEndOfFrame();
        Vector3 directionVector = global.camera.position - transform.position;
        float additionalX = global.random.Next(-100, 100) / 10.0F;
        float additionalY = global.random.Next(-100, 100) / 10.0F;
        Vector2 additionalVector = new Vector2(additionalX, additionalY);
        directionVector = directionVector + additionalVector.ToVector3();
        float distance = (transform.position + directionVector * 1.1F).magnitude;
        float speed = distance * 1.5F;
        directionVector = directionVector.normalized;
        while(distance > 0)
        {
            yield return new WaitForFixedUpdate();
            transform.Translate(directionVector * Time.deltaTime * speed, Space.World);
            distance -= Time.deltaTime * speed;
        }
        Destroy(gameObject);
    }
}
