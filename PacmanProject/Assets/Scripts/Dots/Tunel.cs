﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tunel : MonoBehaviour
{
    Vector2 otherTunel = Vector2.zero;

    void Start ()
    {
        if (transform.position.ToVector2() == global.Maze.Tunel1)
            otherTunel = global.Maze.Tunel2;
        else
            otherTunel = global.Maze.Tunel1;
    }

    private void OnTriggerEnter(Collider other)
    {
        other.transform.position = (otherTunel.ToVector3() + (transform.position - other.transform.position)).ToVector2().ToVector3();
    }
}
