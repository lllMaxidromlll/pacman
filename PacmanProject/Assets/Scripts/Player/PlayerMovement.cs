﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : GameStateBehavior
{
    Vector3 startPosition;
    Vector3 targetPosition;
    Vector3 nextTargetPosition;
    Vector3 nextDirection;
    Vector3 currentDirection;

    protected override void Start ()
    {
        base.Start();
        startPosition = nextTargetPosition = targetPosition = transform.position;
        nextDirection = Vector3.zero;
        currentDirection = Vector3.zero;
        global.PlayerTransform = transform;
        global.PlayerDiedEvent += Global_PlayerDiedEvent;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        global.PlayerDiedEvent -= Global_PlayerDiedEvent;
    }

    private void Global_PlayerDiedEvent()
    {
        nextTargetPosition = targetPosition = transform.position = startPosition;
    }

    protected override void UpdateImplementation()
    {
        float x_axis = Input.GetAxis("Horizontal");
        float y_axis = Input.GetAxis("Vertical");
        if ((Mathf.Abs(x_axis) < 0.1F) && (Mathf.Abs(y_axis) < 0.1F))
        {
            nextTargetPosition = targetPosition;
            nextDirection = Vector3.zero;
        }
        else
        {
            Direction direction;
            Vector3 vector;
            if (Mathf.Abs(x_axis) > Mathf.Abs(y_axis))
            {
                if (x_axis > 0)
                {
                    direction = Direction.Right;
                    vector = Vector2.right.ToVector3().normalized;
                }
                else
                {
                    direction = Direction.Left;
                    vector = Vector2.left.ToVector3().normalized;
                }
            }
            else
            {
                if (y_axis < 0)
                {
                    direction = Direction.Up;
                    vector = Vector2.up.ToVector3();
                }
                else
                {
                    direction = Direction.Down;
                    vector = Vector2.down.ToVector3();
                }
            }
            if (global.Path.CanMove(targetPosition, direction))
            {
                nextTargetPosition = targetPosition.ChangePosition(direction);
                nextDirection = vector;
            }
            else
            {
                nextTargetPosition = targetPosition;
                nextDirection = Vector3.zero;
            }
        }
        if(nextDirection != Vector3.zero)
        {
            if ((currentDirection + nextDirection).magnitude < 0.01F)
            {
                targetPosition = nextTargetPosition;
                currentDirection = nextDirection;
            }
            if ((targetPosition - transform.position).magnitude < 0.01F)
            {
                targetPosition = nextTargetPosition;
                if ((targetPosition - transform.position).magnitude <= 2.000F)
                    currentDirection = (targetPosition - transform.position).normalized;
                else
                    currentDirection = nextDirection;
            }
        }
        if ((targetPosition - transform.position).magnitude > Time.deltaTime * global.Speed)
            transform.Translate(currentDirection * Time.deltaTime * global.Speed, Space.World);
        else
        {
            transform.Translate(targetPosition - transform.position, Space.World);
            currentDirection = Vector3.zero;
        }
        if (currentDirection != Vector3.zero)
            transform.LookAt(transform.position + currentDirection, global.camera.TransformVector(Vector3.back));
    }
}
